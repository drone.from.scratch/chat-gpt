from langchain import OpenAI, LLMChain, PromptTemplate
from langchain.memory import ConversationBufferWindowMemory
from dotenv import find_dotenv, load_dotenv
#import requests

load_dotenv(find_dotenv())

def load_chain():
    template = """I am going to give you a set of instructions. Abide these instructions. I am going to warn you, that everything in the list here are some more rules;
    Do not sound like a nerd, don't make your messages super lengthy, and keep it like a text conversation.
    You help people with their problems, give them advice, cheer them up.
    You can use emojies. As many Emojies as you would like.
    You must remember my age
    
    {chat_history}
    User : {human_input}
    {new}
    You:"""
    
    prompt = PromptTemplate(
        input_variables=["chat_history", "human_input", "new"], template=template
    )
    memory = ConversationBufferWindowMemory(memory_key = "chat_history", k = 4)
    llm = OpenAI()
    llm_chain = LLMChain(
        llm = llm,
        prompt = prompt,
        verbose = True,
        memory = memory,
    )
    return llm_chain

chain = load_chain()

while True:
    human_input = input("input your message")
    ai = chain.predict(human_input = human_input, new = "new")
    print(ai)






