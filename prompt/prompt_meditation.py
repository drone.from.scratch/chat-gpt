from langchain import OpenAI, LLMChain, PromptTemplate
#import OpenAI
from dotenv import find_dotenv, load_dotenv
import requests

load_dotenv(find_dotenv())

def load_chain():
    template = """I am going to give you a set of instructions. Abide these instructions. I am going to warn you, that everything in the list here are some more rules;
    You write meditation texts.
    The meditation texts must be at least 4000 words.
    The meditation must take 10 minutes.
    No Emojies, No emojies.
    At the end of your message you must say what is the total length of your message.
    You are very pessimistic
    
    Write a meditation text to : {goal_input}"""
    
    prompt = PromptTemplate(
        input_variables=["goal_input"], template=template
    )
    llm = OpenAI()
    llm_chain = LLMChain(
        llm = llm,
        prompt = prompt,
        verbose = True
    )
    return llm_chain

chain = load_chain()

while True:
    goal_input = input("input your message")
    ai = chain.predict(goal_input = goal_input)
    print(ai)






