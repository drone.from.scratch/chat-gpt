from langchain.indexes import VectorstoreIndexCreator
from langchain.document_loaders import TextLoader
from langchain.text_splitter import CharacterTextSplitter
from langchain.embeddings import OpenAIEmbeddings
from langchain.vectorstores.chroma import Chroma
from langchain.chains import RetrievalQA

from langchain.memory import ConversationBufferWindowMemory
from langchain.chat_models import ChatOpenAI

from langchain.llms import OpenAI
from langchain.chains import LLMChain
from langchain.prompts import PromptTemplate 

import os
from dotenv import load_dotenv

load_dotenv()

os.environ['OPENAI_API_KEY'] = os.getenv('OPENAI_API_KEY')


def add_database():
    loader = TextLoader('data/data_table.csv')
    documents = loader.load()

    text_splitter = CharacterTextSplitter(chunk_size=4000, chunk_overlap=0)
    texts = text_splitter.split_documents(documents)

    embeddings = OpenAIEmbeddings()
    db = Chroma.from_documents(texts, embeddings)

    retriever = db.as_retriever(search_kwargs={"k": 1})
    qa = RetrievalQA.from_chain_type(llm=ChatOpenAI(model_name="ft:gpt-3.5-turbo-0613:neomind::8Aya8kRz"),
                                     chain_type="stuff",
                                     retriever=retriever)

    return qa


def load_chain():
    template = """
    {chat_history}
    write something funny and then append the following text

    {human_input}
    
    """
    
    prompt = PromptTemplate(
        input_variables = ["human_input", "chat_history"], template=template
    )
    memory = ConversationBufferWindowMemory(memory_key = "chat_history", k = 4)
    llm = ChatOpenAI()
    llm_chain = LLMChain(
        llm = llm,
        prompt = prompt,
        #verbose = True,
        memory = memory,
    )
    return llm_chain


lis = ["sunrise time", "temperature", "precipitation", "wind speed", "visibility"]
qa = add_database()
chain = load_chain()


while True:
    values = []

    date = input("Message: ")
    for element in lis:
        message = "{} {}".format(date, element)
        values.append(qa.run(message))

    #2023-09-01T05:33:23
    index_of_T = values[0].find('T')
    values[0] = values[0][index_of_T + 1:]

    text = """
    🌅 Sunrise time: {0}.
    🌡️ Temperature: {1} °C.
    🌧️ Precipitation: {2} mm.
    💨 Wind speed: {3} km/h.
    👀 Visibility: {4} km.""".format(*values)

    ai = chain.predict(human_input = text)
    
    print(ai, end="\n\n")









    

