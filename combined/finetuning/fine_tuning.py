import os
import openai

from dotenv import load_dotenv

load_dotenv()


openai.api_key = os.getenv("OPENAI_API_KEY")

# Upload
"""
openai.File.create(
  file=open("data.jsonl", "rb"),
  purpose='fine-tune'
)
"""

# Training
openai.FineTuningJob.create(training_file="file-nx24mQJNUHJ2kC9vE4YLOead", model="gpt-3.5-turbo")

# Use

"""
while True:
  a = input("message: ")
  completion = openai.ChatCompletion.create(
    model="ft:gpt-3.5-turbo-0613:neomind::8AsA0uiP",
    messages=[
      {"role": "system", "content": "Dave is a bot that helps with writing about the weather. Every sentence must start on a new line. After every message, you must add a joke."},
      {"role": "user", "content": a}
    ]
  )
  
  print(completion.choices[0].message["content"])
"""
