import openai
import json
import os

from dotenv import load_dotenv

load_dotenv()

openai.api_key = os.getenv("OPENAI_API_KEY")

# Example dummy function hard coded to return the same weather
# In production, this could be your backend API or an external API
def rep_sad():
    sad = {
        "answer": "Have a candy",
    }
    return json.dumps(sad)


def rep_happy():
    happy = {
        "answer": "Write about today's day in your diary",
    }
    return json.dumps(happy)

def run_conversation(text):
    # Step 1: send the conversation and available functions to GPT
    messages = [{"role": "user", "content": text}]

    functions = [
        {
            "name": "rep_sad",
            "description": "Reply when person is sad",
            "parameters": { "type": "object", "properties": {}}
        },
        {
            "name": "rep_happy",
            "description": "Reply when person is happy",
            "parameters": { "type": "object", "properties": {}}
        }
    ]
    response = openai.ChatCompletion.create(
        model="gpt-3.5-turbo-0613",
        messages=messages,
        functions=functions,
        function_call="auto",  # auto is default, but we'll be explicit
    )
    response_message = response["choices"][0]["message"]

    # Step 2: check if GPT wanted to call a function
    if response_message.get("function_call"):
        # Step 3: call the function
        # Note: the JSON response may not always be valid; be sure to handle errors
        available_functions = {
            "rep_sad": rep_sad,
            "rep_happy": rep_happy,
        }  # only one function in this example, but you can have multiple
        function_name = response_message["function_call"]["name"]
        function_to_call = available_functions[function_name]
        function_args = json.loads(response_message["function_call"]["arguments"])
        function_response = function_to_call()

        # Step 4: send the info on the function call and function response to GPT
        messages.append(response_message)  # extend conversation with assistant's reply
        messages.append(
            {
                "role": "function",
                "name": function_name,
                "content": function_response,
            }
        )  # extend conversation with function response
        second_response = openai.ChatCompletion.create(
            model="gpt-3.5-turbo-0613",
            messages=messages,
        )  # get a new response from GPT where it can see the function response
        return second_response["choices"][0]["message"]["content"]


while True:
    text = input("Message: ")
    print(run_conversation(text))


