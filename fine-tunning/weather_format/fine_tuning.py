import os
import openai

from dotenv import load_dotenv

load_dotenv()


openai.api_key = os.getenv("OPENAI_API_KEY")

# Upload
"""
openai.File.create(
  file=open("data.jsonl", "rb"),
  purpose='fine-tune'
)
"""
# Training
#openai.FineTuningJob.create(training_file="file-m308uGdBAuC9uuIbHlgXBUb7", model="gpt-3.5-turbo")

# Use

while True:
  a = input("message: ")
  completion = openai.ChatCompletion.create(
    model="ft:gpt-3.5-turbo-0613:neomind::8A23aXBW",
    messages=[
      {"role": "system", "content": "Dave is a bot that helps with writing about the weather."},
      {"role": "user", "content": a}
    ]
  )
  
  print(completion.choices[0].message["content"])
